#pragma once

#include "Array.hpp"
#include "Config.hpp"
#include "MetaFunc.hpp"
#include "SimpleBLAS.hpp"
#include "SparseVector.hpp"

namespace cppkit
{
    enum class StatusQMR : int
    {
        SUCCESS = 0,
        BREAKDOWN_RHO = -1,
        BREAKDOWN_BETA = -2,
        BREAKDOWN_GAMMA = -3,
        BREAKDOWN_DELTA = -4,
        BREAKDOWN_EPSILON = -5,
        BREAKDOWN_XI = -6,
        EXCEED = -99
    };

    template<typename T>
    class SolverQMR
    {
        using vec_t = Vector<T>;

    private:
        vec_t r, v_tld, y, w_tld, z;
        vec_t v, w, y_tld, z_tld;
        vec_t p, q, p_tld, d, s;

        Real rho[2], xi, gamma[2], theta[2];
        T eta, delta, epsilon, beta;

        Integer max_iter;
        Real tolerence;

    public:
        inline SolverQMR() = default;
        ~SolverQMR() = default;

        inline explicit SolverQMR(Integer n)
            : r(vec_t(n))
            , v_tld(vec_t(n))
            , y(vec_t(n))
            , w_tld(vec_t(n))
            , z(vec_t(n))
            , v(vec_t(n))
            , w(vec_t(n))
            , y_tld(vec_t(n))
            , z_tld(vec_t(n))
            , p(vec_t(n))
            , q(vec_t(n))
            , p_tld(vec_t(n))
            , d(vec_t(n))
            , s(vec_t(n))
            , max_iter(2000)
            , tolerence(1.0E-7)
        {
        }

        inline void setParam(Integer imax, Real tol)
        {
            max_iter = imax;
            tolerence = tol;
        }

        /************************************
         Two term recurrence version without look-ahead
         Implement details: 1) qmr.m In Matlab;
                            2) cpptemplates:
        http://www.netlib.org/templates/index.html
        ************************************/
        template<typename CoefMatrix, typename PrecMatrix>
        StatusQMR solve(const CoefMatrix &A, const PrecMatrix &M, vec_t &x,
                        const vec_t &b, RvSpVec &report)
        {
            Integer iter = 0;
            Real err;
            report = RvSpVec(1 + max_iter);
            StatusQMR status = StatusQMR::SUCCESS;

            Real normb = cppkit::norm(b);
            if (normb == 0.0) normb = 1.0;

            r = b - A * x;
            err = cppkit::norm(r) / normb;

            report = {iter, iter, err};

            if (err > tolerence)
            {
                v_tld = r;
                M.solveLower(y, v_tld);
                rho[0] = cppkit::norm(y);

                w_tld = r;
                M.solveTransUpper(z, w_tld);
                xi = cppkit::norm(z);

                gamma[0] = 1.0;
                eta = -1.0;
                theta[0] = 0.0;

                for (iter = 1; iter <= max_iter; ++iter)
                {
                    if (rho[0] == 0.0)
                    {
                        status = StatusQMR::BREAKDOWN_RHO;
                        break;
                    }

                    if (xi == 0.0)
                    {
                        status = StatusQMR::BREAKDOWN_XI;
                        break;
                    }

                    v = 1.0 / rho[0] * v_tld;
                    y = 1.0 / rho[0] * y;

                    w = 1.0 / xi * w_tld;
                    z = 1.0 / xi * z;

                    if ((delta = cppkit::dotc(z, y)) == 0.0)
                    {
                        status = StatusQMR::BREAKDOWN_DELTA;
                        break;
                    }

                    M.solveUpper(y_tld, y);
                    M.solveTransLower(z_tld, z);

                    if (iter > 1)
                    {
                        p = y_tld - (xi * delta / epsilon) * p;
                        q = z_tld
                            - (rho[0] * cppkit::conj(delta / epsilon)) * q;
                    }
                    else
                    {
                        p = y_tld;
                        q = z_tld;
                    }

                    p_tld = A * p;
                    if ((epsilon = cppkit::dotc(q, p_tld)) == 0.0)
                    {
                        status = StatusQMR::BREAKDOWN_EPSILON;
                        break;
                    }

                    if ((beta = epsilon / delta) == 0.0)
                    {
                        status = StatusQMR::BREAKDOWN_BETA;
                        break;
                    }

                    v_tld = p_tld - beta * v;
                    M.solveLower(y, v_tld);

                    rho[1] = rho[0];
                    rho[0] = cppkit::norm(y);

                    A.transMultiplyVector(q, w_tld);
                    w_tld -= cppkit::conj(beta) * w;

                    M.solveTransUpper(z, w_tld);
                    xi = cppkit::norm(z);

                    gamma[1] = gamma[0];
                    theta[1] = theta[0];

                    theta[0] = rho[0] / (gamma[1] * cppkit::abs(beta));
                    gamma[0] = 1.0 / cppkit::sqrt(1.0 + theta[0] * theta[0]);

                    if (gamma[0] == 0.0)
                    {
                        status = StatusQMR::BREAKDOWN_GAMMA;
                        break;
                    }

                    eta = -eta * rho[1] * gamma[0] * gamma[0]
                          / (beta * gamma[1] * gamma[1]);

                    if (iter > 1)
                    {
                        Real tc = theta[1] * theta[1] * gamma[0] * gamma[0];
                        d = eta * p + tc * d;
                        s = eta * p_tld + tc * s;
                    }
                    else
                    {
                        d = eta * p;
                        s = eta * p_tld;
                    }

                    x += d;
                    r -= s;

                    err = cppkit::norm(r) / normb;
                    report = {iter, iter, err};

                    if (err <= tolerence) break;
                }
            }

            if (iter > max_iter)
            {
                status = StatusQMR::EXCEED;
                --iter;
            }
            report = report.resize(iter + 1);

            return status;
        }
    };

}  // namespace cppkit
