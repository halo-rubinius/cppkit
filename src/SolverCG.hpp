#pragma once

#include "Array.hpp"
#include "Config.hpp"
#include "SimpleBLAS.hpp"
#include "SparseVector.hpp"

namespace cppkit
{
    enum class StatusCG : int
    {
        SUCCESS = 0,
        ERROR = -1,
        EXCEED = -99  // Exceed max iteration
    };

    template<typename T>
    class SolverCG
    {
        using vec_t = Vector<T>;

    private:
        vec_t p, q, z, r;
        Integer max_iter;
        Real tolerance;

    public:
        inline SolverCG() = default;
        ~SolverCG() {}

        inline explicit SolverCG(Integer n)
            : p(vec_t(n))
            , q(vec_t(n))
            , z(vec_t(n))
            , r(vec_t(n))
            , max_iter(1000)
            , tolerance(1.0E-8)
        {
        }

        /** 设置迭代参数
         * @param imax 最大迭代次数
         * @param tol 收敛误差限
         */
        inline void setParam(Integer imax, Real tol)
        {
            max_iter = imax;
            tolerance = tol;
        }

        /** 运用共轭梯度方法，求解线性方程组
         * cpptemplates: http://www.netlib.org/templates/index.html
         * @param A 系数矩阵
         * @param M 预处理矩阵
         * @param x 未知向量
         * @param b 右端项
         * @param report 迭代信息，存储迭代过程中的误差
         */
        template<typename CoefMatrix, typename PrecMatrix>
        StatusCG solve(const CoefMatrix &A, const PrecMatrix &M, vec_t &x,
                       const vec_t &b, RvSpVec &report)
        {
            StatusCG status = StatusCG::SUCCESS;
            report = RvSpVec(max_iter + 1);

            Integer iter = 0;

            Real bNorm = cppkit::norm(b);
            if (bNorm == 0.0) bNorm = 1.0;

            r = b - A * x;
            Real rNorm = cppkit::norm(r);

            Real err = rNorm / bNorm;

            report = {0, iter, err};

            if (err > tolerance)
            {
                T rho[2], alpha, beta;
                for (iter = 1; iter <= max_iter; ++iter)
                {
                    M.solve(z, r);

                    rho[1] = rho[0];
                    rho[0] = cppkit::dot(r, z);

                    if (iter == 1) { p = z; }
                    else
                    {
                        beta = rho[0] / rho[1];
                        p = z + beta * p;
                    }

                    q = A * p;
                    alpha = rho[0] / cppkit::dot(p, q);

                    x += alpha * p;
                    r -= alpha * q;

                    rNorm = cppkit::norm(r);
                    err = rNorm / bNorm;

                    report = {iter, iter, err};

                    if (err <= tolerance) break;
                }
            }

            if (iter > max_iter)
            {
                status = StatusCG::EXCEED;
                --iter;
            }
            report = report.resize(iter + 1);

            return status;
        }
    };

}  // namespace cppkit
