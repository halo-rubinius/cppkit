#pragma once

namespace cppkit
{
    template<typename T>
    struct is_binary_operator
    {
        static constexpr bool value = false;
    };


    struct OpPlus
    {
        template<typename Left, typename Right>
        inline static constexpr auto apply(const Left &lhs, const Right &rhs)
        {
            return lhs + rhs;
        }
    };

    template<>
    struct is_binary_operator<OpPlus>
    {
        static constexpr bool value = true;
    };


    struct OpMinus
    {
        template<typename Left, typename Right>
        inline static constexpr auto apply(const Left &lhs, const Right &rhs)
        {
            return lhs - rhs;
        }
    };

    template<>
    struct is_binary_operator<OpMinus>
    {
        static constexpr bool value = true;
    };


    struct OpMultiply
    {
        template<typename Left, typename Right>
        inline static constexpr auto apply(const Left &lhs, const Right &rhs)
        {
            return lhs * rhs;
        }
    };

    template<>
    struct is_binary_operator<OpMultiply>
    {
        static constexpr bool value = true;
    };


    struct OpDivide
    {
        template<typename Left, typename Right>
        inline static constexpr auto apply(const Left &lhs, const Right &rhs)
        {
            return lhs / rhs;
        }
    };

    template<>
    struct is_binary_operator<OpDivide>
    {
        static constexpr bool value = true;
    };

}  // namespace cppkit
