// All-In-One header

#pragma once

#include "Config.hpp"

#include "Complex.hpp"

#include "Constant.hpp"

#include "Clock.hpp"

#include "MetaFunc.hpp"

#include "VectorExpression.hpp"

#include "Array.hpp"
#include "SimpleBLAS.hpp"

#include "BandMatrix.hpp"
#include "SparseMatrix.hpp"
#include "SparseVector.hpp"

#include "Mesh.hpp"
#include "Station.hpp"

#include "SolverCG.hpp"
#include "SolverQMR.hpp"


using namespace cppkit;
