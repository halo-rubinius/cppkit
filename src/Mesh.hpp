#pragma once

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "Array.hpp"
#include "Config.hpp"
#include "MetaFunc.hpp"

namespace cppkit
{
    inline Array<Real, 1> CumSpace(const Array<Real, 1> &Dx)
    {
        Integer Nx = Dx.size();
        Array<Real, 1> Cx(Nx + 1);

        Cx(0) = 2.0 * Dx(0);
        for (Integer ix = 1; ix < Nx; ++ix)
        {
            Cx(ix) = Dx(ix) + Dx(ix - 1);
        }
        Cx(Nx) = 2.0 * Dx(Nx);
        return Cx;
    }

    inline Array<Real, 1> GridLine(Real X0, const Array<Real, 1> &Dx)
    {
        Integer Nx = Dx.size();
        Array<Real, 1> Gx(Nx + 1);

        Gx(0) = X0;
        for (Integer ix = 1; ix < Nx + 1; ++ix)
        {
            Gx(ix) = Gx(ix - 1) + Dx(ix - 1);
        }
        return Gx;
    }

    class Mesh
    {
    public:
        Integer NzAir;              // 空气层层数
        Real Ox, Oy;                // 中心点坐标
        Array<Real, 1> Dx, Dy, Dz;  // 网格间距
        Array<Real, 1> Gx, Gy, Gz;  // 网格线坐标

        inline Mesh() : NzAir(0), Ox(0.0), Oy(0.0) {}

        inline explicit Mesh(Integer nz, Integer nza = 0, Integer ny = 0,
                             Integer nx = 0)
            : NzAir(nza)
            , Ox(0.0)
            , Oy(0.0)
            , Dz(Array<Real, 1>(nz))
            , Dy(Array<Real, 1>(ny))
            , Dx(Array<Real, 1>(nx))
        {
        }

        inline ~Mesh() { NzAir = 0; }

        inline void setGrid()
        {
            if (!Dx.isEmpty()) { Gx = GridLine((Ox - Dx.sum()) / 2.0, Dx); }
            if (!Dy.isEmpty()) { Gy = GridLine((Oy - Dy.sum()) / 2.0, Dy); }
            if (!Dz.isEmpty())
            {
                Gz = NzAir > 0 ? GridLine(-Dz.sum(0, NzAir - 1), Dz)
                               : GridLine(0.0, Dz);
            }
        }

        inline Integer dim() const noexcept
        {
            Integer i = 0;
            if (!Dz.isEmpty()) ++i;
            if (!Dy.isEmpty()) ++i;
            if (!Dx.isEmpty()) ++i;
            return i;
        }

        inline void import(std::istream &is)
        {
            std::string str;

            cppkit::getline(is, str);
            cppkit::getline(is, str);
            is >> Dx;
            cppkit::getline(is, str);
            is >> Dy;
            cppkit::getline(is, str);
            is >> Dz >> NzAir >> std::ws;
            cppkit::getline(is, str);

            setGrid();
        }

        inline void import(std::string file)
        {
            std::fstream fs;
            fs.open(file, std::fstream::in);
            import(fs);
            fs.close();
        }

        friend std::ostream &operator<<(std::ostream &os, const Mesh &obj)
        {
            os << ">>MESH INFO" << std::endl;

            os << " X GRID:" << std::endl;
            os << obj.Dx;

            os << " Y GRID:" << std::endl;
            os << obj.Dy;

            os << " Z GRID & NZAIR:" << std::endl;
            os << obj.Dz << "  " << obj.NzAir << std::endl;

            os << ">>END MESH" << std::endl;

            return os;
        }

        friend std::istream &operator>>(std::istream &is, Mesh &obj)
        {
            obj.import(is);
            return is;
        }
    };

}  // namespace cppkit
