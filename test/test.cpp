#include <iostream>
#include <random>
#include "cppkit.hpp"

std::random_device rd;
std::mt19937 engine(rd());
std::uniform_real_distribution<double> distribution(-1.0, 1.0);

constexpr int N = 1000000, REP = 10000;
double a = distribution(engine), b = distribution(engine),
       c = distribution(engine), d = distribution(engine);

using Complex = complex<double>;

void testConstant()
{
    std::cout << PI << "  " << MU0 << "  " << AIR_COND << "  " << RATIO_DEG2RAD
              << "  " << RATIO_RAD2DEG << std::endl;
}

void testClock()
{
    double *x = new double[N], *y = new double[N];
    for (int i = 0; i < N; ++i)
    {
        x[i] = a;
        y[i] = b;
    }

    double r = 0.0;
    Clock timer;
    timer.tic();
    for (int k = 0; k < REP; ++k)
    {
        for (int i = 0; i < N; ++i)
        {
            r += x[i] * y[i];
        }
    }
    double t = timer.toc();
    std::cout << "Result: " << r << std::endl << "Time: " << t << std::endl;

    delete[] x;
    delete[] y;
}

void testComplex()
{
    Complex z1(a, b), z2(c, d);

    std::cout << "z1: " << z1 << std::endl << "z2: " << z2 << std::endl;
    std::cout << "z1 + z2: " << z1 + z2 << std::endl
              << "z1 - z2: " << z1 - z2 << std::endl
              << "z1 * z2: " << z1 * z2 << std::endl
              << "z1 / z2: " << z1 / z2 << std::endl;


    std::cout << conj(1.2) << "  " << conj(z1) << std::endl;
}

void foo(double &a) { a += 1; }

void testArray()
{
    double *x0 = new double[N], *y0 = new double[N];
    Array<double, 1> x1(N), y1(N);

    for (int i = 0; i < N; ++i)
    {
        x0[i] = a;
        y0[i] = b;
        x1(i) = a;
        y1(i) = b;
    }

    double r = 0.0, t;
    Clock timer;
    timer.tic();
    for (int k = 0; k < REP; ++k)
    {
        for (int i = 0; i < N; ++i)
        {
            r += x0[i] * y0[i];
        }
    }
    t = timer.toc();
    std::cout << "Result (C): " << r << std::endl << "Time: " << t << std::endl;

    delete[] x0;
    delete[] y0;

    r = 0.0;
    timer.tic();
    for (int k = 0; k < REP; ++k)
    {
        for (int i = 0; i < N; ++i)
        {
            r += x1(i) * y1(i);
        }
    }
    t = timer.toc();
    std::cout << "Result (Array): " << r << std::endl
              << "Time: " << t << std::endl;

    Array<double, 1> tp(3);
    tp = 2.3;
    std::cout << tp.evaluate(exp);
    std::cout << tp.apply(foo);
}


void testBLAS()
{
    Array<double, 1> x(N), y(N);
    x = a;
    y = b;
    std::cout << "For double:" << std::endl;
    std::cout << a << "  " << b << std::endl
              << dot(x, y) << std::endl
              << norm(x) << std::endl;

    std::cout << "For complex:" << std::endl;
    Vector<Complex> z1(N), z2(N);
    z1 = Complex(a, b);
    z2 = Complex(c, d);
    std::cout << z1(0) << std::endl
              << z2(0) << std::endl
              << dot(z1, z2) << std::endl
              << dotc(z1, z2) << std::endl
              << norm(z1) << std::endl;
}

void testSparseVector()
{
    SparseVector<double> spv(2);
    spv = {0, 2, 0.3};
    spv = {1, 4, 0.8};

    Vector<double> v(5);
    v = 1.2;

    std::cout << "spv: " << std::endl << spv;
    std::cout << "v * spv: " << v * spv << "  "
              << "spv * v: " << spv * v << std::endl
              << "v += spv: " << (v += spv);
}

void testMesh()
{
    Mesh mesh;
    mesh.import("mesh.dat");

    std::cout << mesh;
}

void testStation()
{
    Mesh mesh;
    mesh.import("mesh.dat");

    Station station;
    station.import("station.dat");
    station.setMeshIndex(mesh);
    std::cout << station;
    std::cout << station.IndexInMesh;
}

void testSparseMatrix()
{
    SparseMatrix<double> A(5, 11);

    A.row(0) = 0;
    A.row(1) = 2;
    A.row(2) = 5;
    A.row(3) = 7;
    A.row(4) = 9;
    A.row(5) = 11;

    A.diag(0) = 0;
    A.diag(1) = 3;
    A.diag(2) = 6;
    A.diag(3) = 7;
    A.diag(4) = 10;

    A.col(0) = 0;
    A.col(1) = 1;
    A.col(2) = 0;
    A.col(3) = 1;
    A.col(4) = 2;
    A.col(5) = 1;
    A.col(6) = 2;
    A.col(7) = 3;
    A.col(8) = 4;
    A.col(9) = 3;
    A.col(10) = 4;

    A.value(0) = 2.3;
    A.value(1) = 3.4;
    A.value(2) = 1.1;
    A.value(3) = 1.2;
    A.value(4) = 1.3;
    A.value(5) = -1.5;
    A.value(6) = -3.4;
    A.value(7) = -1.6;
    A.value(8) = 5.2;
    A.value(9) = 2.2;
    A.value(10) = 2.7;

    std::cout << "Matrix A:" << std::endl << A.denseMatrix();

    Vector<double> b(5);
    b(0) = 1.9;
    b(1) = 6.5;
    b(2) = 0.8;
    b(3) = 8.5;
    b(4) = 4.9;

    std::cout << "Vector b:" << std::endl << b;

    Vector<double> x(5);
    A.solve(x, b);

    std::cout << "Vector x:" << std::endl << x;

    A.solveLower(x, b);
    std::cout << "solveLower:" << std::endl << x;

    A.solveUpper(x, b);
    std::cout << "solveUpper:" << std::endl << x;

    A.solveTransLower(x, b);
    std::cout << "solveTransLower:" << std::endl << x;

    A.solveTransUpper(x, b);
    std::cout << "solveTransUpper:" << std::endl << x;

    A.multiplyVector(b, x);
    std::cout << "multiplyVector:" << std::endl << b << x;

    SparseMatrix<double> M(5, 11);

    A.decompose(M);

    std::cout << "ILU0:" << M.denseMatrix();

    x = A * (b + b);
    std::cout << "A * (b + b) = " << std::endl << x;

    A.multiplyVector(b, x);
    std::cout << "A.multiplyVector(b, x)" << std::endl << x;
}

void testExpressionTemplate()
{
    Vector<double> x1(N), x2(N), x3(N), x4(N), x5(N), x6(N);

    x1 = 1.2;
    x2 = 2.3;
    x3 = 3.4;

    x4 = x1 + x2 * x3 - x1 / x3;

    x5 = 2.3 * x1 + (1.4 + x2) + (1.1 - x3) + (1.5 / x1);

    x6 = x1 * 2.3 + (x2 + 1.4) + (x3 - 1.1) + (x1 / 1.5);

    std::cout << x4(0) << "  " << x5(0) << "  " << x6(0) << "  " << std::endl;
}

int main(int argc, char const *argv[])
{
    std::cout << "选择测试例子:" << std::endl;
    std::cout << "    "
              << "1. 测试Clock" << std::endl;
    std::cout << "    "
              << "2. 测试Constant" << std::endl;
    std::cout << "    "
              << "3. 测试Complex" << std::endl;
    std::cout << "    "
              << "4. 测试Array" << std::endl;
    std::cout << "    "
              << "5. 测试SimpleBLAS" << std::endl;
    std::cout << "    "
              << "6. 测试SparseVector" << std::endl;
    std::cout << "    "
              << "7. 测试Mesh" << std::endl;
    std::cout << "    "
              << "8. 测试Station" << std::endl;
    std::cout << "    "
              << "9. 测试SparseMatrix" << std::endl;
    std::cout << "    "
              << "10. 测试Expression Template" << std::endl;


    int selection;
    std::cin >> selection;
    switch (selection)
    {
        case 1:
            testClock();
            break;
        case 2:
            testConstant();
            break;
        case 3:
            testComplex();
            break;
        case 4:
            testArray();
            break;
        case 5:
            testBLAS();
            break;
        case 6:
            testSparseVector();
            break;
        case 7:
            testMesh();
            break;
        case 8:
            testStation();
            break;
        case 9:
            testSparseMatrix();
            break;
        case 10:
            testExpressionTemplate();
            break;
        default:
            std::cout << "Happy testing." << std::endl;
            break;
    }

    return 0;
}
